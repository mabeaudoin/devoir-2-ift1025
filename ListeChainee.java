/**
 * classe représentant une liste chainée d'objets implémentant l'interface
 * {@link Liste}
 * 
 * @author Etienne Boucher et Marc-André Beaudoin
 *
 */
public class ListeChainee implements Liste {

	Noeud premier;
	Noeud dernier;
	int n;

	public ListeChainee() {
		n = 0;
		premier = null;
		dernier = null;
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public void append(Object o) {
		if (premier == null) {
			premier = new Noeud(o, null);
			dernier = premier;
		} else {
			dernier.setSuivant(new Noeud(o, null));
			dernier = dernier.getSuivant();
		}
		n++;
	}

	@Override
	public void preprend(Object o) {
		if (premier == null) {
			premier = new Noeud(o, null);
			dernier = premier;
		} else {
			premier = new Noeud(o, premier);
		}
		n++;
	}

	@Override
	public Object getFirst() {
		return premier.getContenu();
	}

	@Override
	public Object getLast() {
		return dernier.getContenu();
	}

	@Override
	public Object removeFirst() {
		Object first = premier.getContenu();
		premier = premier.getSuivant();
		n--;
		return first;
	}

	@Override
	public Object get(int position) {
		if (position < 0 || position > n) {
			throw new ArrayIndexOutOfBoundsException("position hors chaine");
		}
		return this.getNoeud(position).getContenu();
	}

	@Override
	public void set(int position, Object o) {
		if (position < 0 || position > n) {
			throw new ArrayIndexOutOfBoundsException("position hors chaine");
		}
		Noeud precedent = this.getNoeud(position - 1);
		precedent.setSuivant(new Noeud(o, precedent.getSuivant()));
	}

	/**
	 * renvoie l'adresse du Noeud situé dans la ListeChainee à la position
	 * donnée en paramètre
	 * 
	 * @param position
	 *            position dans la ListeChainee du noeud recherché
	 * @return Noeud situé à la position fournie en paramètre
	 */
	public Noeud getNoeud(int position) {
		if (position < 0 || position > n) {
			throw new ArrayIndexOutOfBoundsException("position hors chaine");
		}
		Noeud noeud = premier;
		// TODO vérification de l'implémentation
		for (int i = 0; i < position; i++) {
			noeud = noeud.getSuivant();
		}
		return noeud;
	}

	@Override
	public String toString() {
		String string = "ListeChainee[ ";
		for (Noeud n = premier; n.getSuivant() != null; n = n.getSuivant()) {
			string += n.getContenu() + ", ";
		}
		return string + dernier.getContenu() + " ]";
	}

}