
/**
 * Classe representant un Joueur
 * 
 * @author Etienne Boucher et Marc-Andre Beaudoin
 *
 */
import java.awt.Color;

abstract class Joueur {
	protected Trace trace;
	protected Color couleur;
	protected boolean vivant;
	protected boolean inGame;
	protected char direction_courante;
	protected char nouvelle_direction;

	/**
	 * @return la Trace du Joueur
	 */
	public Trace getTrace() {
		return trace;
	}

	/**
	 * @return couleur du joueur
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * @param couleur
	 *            couleur a appliquer au joueur
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

	/**
	 * @return true si le Joueur est vivant, false sinon
	 */
	public boolean isVivant() {
		return vivant;
	}

	/**
	 * @param vivant
	 *            l'etat de vie a appliquer au Joueur
	 */
	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}

	/**
	 * @return true si le Joueur fait partie de la partie
	 */
	public boolean isInGame() {
		return inGame;
	}

	/**
	 * @return la direction courante du Joueur
	 */
	public char getDirection_courante() {
		return direction_courante;
	}

	/**
	 * @param direction_courante
	 *            la direction courante a appliquer au Joueur
	 */
	public void setDirection_courante(char direction_courante) {
		this.direction_courante = direction_courante;
	}

	/**
	 * @return la nouvelle direction du Joueur
	 */
	public char getNouvelle_direction() {
		return nouvelle_direction;
	}

	/**
	 * @param nouvelle_direction
	 *            la nouvelle direction a appliquer au Joueur
	 */
	public void setNouvelle_direction(char nouvelle_direction) {
		this.nouvelle_direction = nouvelle_direction;
	}

}
