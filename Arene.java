import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.*;
import javax.swing.BorderFactory;
import javax.swing.JComponent;

/**
 * JComponent qui affiche la surface de jeu de Tron.
 * 
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class Arene extends JComponent {

	// Variables
	private static final long serialVersionUID = 1L;
	private int largeur_grille;
	private int hauteur_grille;
	private double heightUnite;
	private double widthUnite;
	private Joueur[] joueurs;
	private int nbSquares = 8;

	/**
	 * Constructeur d'objets de type Arene
	 * 
	 * @param largeur
	 *            largeur de la zone de jeu
	 * @param hauteur
	 *            hauteur de la zone de jeu
	 */
	public Arene(int largeur, int hauteur, Joueur[] joueurs) {
		this.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		this.setBackground(Color.black);
		largeur_grille = largeur;
		hauteur_grille = hauteur;
		this.setPreferredSize(new Dimension(largeur, hauteur));
		widthUnite = (double) getWidth() / largeur_grille;
		heightUnite = (double) getHeight() / hauteur_grille;

		this.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				widthUnite = (double) getWidth() / largeur_grille;
				heightUnite = (double) getHeight() / hauteur_grille;
			}
		});

		this.joueurs = joueurs;
	}

	@Override
	public void paintComponent(Graphics g) {
		// impression du background et de la grille
		g.setColor(Color.black);
		g.fillRect(0, 0, (int) (widthUnite * largeur_grille), (int) (heightUnite * hauteur_grille));

		int squareWidth = ((int) (getWidth()) / nbSquares);
		int squareHeight = ((int) (getHeight()) / nbSquares);
		for (int i = 0; i < nbSquares; i++) {
			for (int j = 0; j < nbSquares; j++) {
				{
					g.setColor(new Color(24, 202, 230, 30));
					g.drawRect(i * squareWidth + 1, j * squareHeight + 1, squareWidth, squareHeight);
				}
			}
		}

		// Définission des unités de dessin
		int largeurDessin = ((int) widthUnite == 0) ? 1 : (int) widthUnite;
		int hauteurDessin = ((int) heightUnite == 0) ? 1 : (int) heightUnite;

		// Dessin des Traces
		for (int i = 0; i < 3; i++) {
			g.setColor(joueurs[i].getCouleur());
			for (int j = 0; j < joueurs[i].getTrace().length(); j++) {
				Point debut = joueurs[i].getTrace().get(j).getDebut();
				Point fin = joueurs[i].getTrace().get(j).getFin();
				switch (TronControlPanel.direction(debut, fin)) {
				case 'N':
				case 'O':
					g.fillRect((int) (debut.getX() * widthUnite - 1), (int) (debut.getY() * heightUnite - 1),
							largeurDessin + 1, hauteurDessin + 1);
					break;
				case 'S':
				case 'E':
					g.fillRect((int) (fin.getX() * widthUnite - 1), (int) (fin.getY() * heightUnite - 1),
							largeurDessin + 1, hauteurDessin + 1);
					break;
				}
			}
		}
	}
}