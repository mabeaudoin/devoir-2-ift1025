/**
 * Classe representant un segment de droite reliant 2 Points
 * 
 * @author Etienne Boucher et Marc-Andre Beaudoin
 *
 */
public class Segment {
	private Point debut, fin;

	/**
	 * Constructeur d'un Segment de debut a fin
	 * 
	 * @param debut
	 *            position du debut du segment
	 * @param fin
	 *            position de la fin du segment
	 */
	public Segment(Point debut, Point fin) {
		this.debut = debut;
		this.fin = fin;
	}

	/**
	 * Accesseur de la position du debut du segment
	 * 
	 * @return Point de la position du debut du segment
	 */
	public Point getDebut() {
		return debut;
	}

	/**
	 * Modificateur de la position du debut du segment
	 * 
	 * @param debut
	 *            position du debut a attribuer au Segment
	 */
	public void setDebut(Point debut) {
		this.debut = debut;
	}

	/**
	 * Accesseur de la position de la fin du segment
	 * 
	 * @return Point de la position de la fin du segment
	 */
	public Point getFin() {
		return fin;
	}

	/**
	 * Modificateur de la position de la fin du segment
	 * 
	 * @param fin
	 *            position de fin a attribuer au Segment
	 */
	public void setFin(Point fin) {
		this.fin = fin;
	}

	@Override
	public String toString() {
		return "{" + debut + ',' + fin + "}";
	}
}