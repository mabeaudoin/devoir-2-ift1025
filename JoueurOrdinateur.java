import java.awt.Color;

/**
 * Classe de JoueurOrdinateur, extension de la classe abstraite {@link Joueur}.
 * Implemente l'intelligence artificielle su joueur ordinateur.
 * 
 * @author Marc-andre Beaudoin et Etienne Boucher
 *
 */
public class JoueurOrdinateur extends Joueur {

	// Timer universel pour le JoueurOrdinateur
	public static int timerAction = 0;

	/**
	 * Constructeur du JoueurOrdinateur (lorsque le JoueurOrdinateur est inclu
	 * dans la partie)
	 * 
	 * @param tete
	 *            premier segment de la trace
	 * @param direction
	 *            direction initiale
	 * @param couleur
	 *            couleur du trait du JoueurOrdinateur
	 */
	public JoueurOrdinateur(Segment tete, char direction, Color couleur) {
		trace = new Trace(tete);
		direction_courante = direction;
		nouvelle_direction = direction;
		this.couleur = couleur;
		this.vivant = true;
		this.inGame = true;	
	}

	/**
	 * Constructeur du JoueurOrdinateur (lorsque le JoueurOrdinateur n'est pas
	 * inclu dans la partie)
	 * 
	 */
	public JoueurOrdinateur() {
		trace = new Trace();
		direction_courante = '\u0000';
		this.couleur = null;
		this.vivant = false;
		this.inGame = false;
	}

	/**
	 * Methode de l'intelligence artificielle du joueur ordinateur. L'AI se
	 * comporte soit en mode agressif lorsqu'il n'est pas contrait par des
	 * traces ou des murs, soit en mode defensif lorssqu'il se trouves contraint
	 * par une trace ou un mur. Le mecanisme est régit par le parametre
	 * "distCritique" qui est la distance a laquelle l'AI commence a se
	 * comporter de facon defensive pres d'un mur ou d'une trace et par le
	 * parametre "temps de reaction" qui definie la frequence a laquelle l'AI
	 * peut prendre une decision (a chaque x cycles de timer)
	 * 
	 * @param joueurs
	 *            Tableau de joueurs dont l'element en position 0 est le joueur
	 *            ordinateur
	 * @param dimensions
	 *            dimension de l'arene
	 * @return la direction a prendre par l'ordinateur
	 */
	public static char computerAI(Joueur[] joueurs, Point dimensions, char directionAI) {

		// Variables du joueur ordinateur pour l'intelligence artificielle
		char direction = '\u0000';
		int distCritique = 10; // Distance à laquelle l'ordinateur passe en mode
								// defensif
		int tempsReaction = 3; // L'ordinateur ne peut rectifier sa trajectoire
								// que sur un cycle de "tempsReaction", ca donne
								// une chance au joueur!
		int posX_AI = joueurs[0].getTrace().tete().getX();
		int posY_AI = joueurs[0].getTrace().tete().getY();
		boolean modeDefensif = false;

		// Variables de distance avec obstacles et voisins
		int[] distanceObstacle = new int[] { 1000, 1000, 1000, 1000 }; // {N,S,O,E}
																		// Initialises
																		// avec
																		// des
																		// valeurs
																		// arbitrairement
																		// grandes
		double[] distanceJoueurs = new double[2]; // Joueur1, Joueur2

		// Prend une décision periodiquement, selon l'état ou est rendu le
		// timerAction (rend l'AI moins prévisible)
		if (timerAction == 0) {
			// Reinitialisation du timer
			timerAction = tempsReaction;

			// Boucle qui cherche les obstacles (traces ou murs) les plus pres
			// dans les 4 directions
			for (int i = 0; i < 3; i++) {

				// La variable scanInit sert a ce que le joueur ordinateur ne
				// scan qu'apres la distance critique dans sa trace
				int scanInit = distCritique;
				if (i > 0)
					scanInit = 0;

				if (joueurs[i].isInGame()) {
					for (int j = scanInit; j < joueurs[i].getTrace().length(); j++) {

						// DIRECTION NORD
						// Si une trace est plus près du jour que du mur, c'est
						// la trace qui est pris en consideration comme prochain
						// obstacle
						if (joueurs[i].getTrace().get(j).getDebut().getX() == posX_AI // sur
																						// meme
																						// axe
																						// Nord-Sud
								&& joueurs[i].getTrace().get(j).getDebut().getY() < posY_AI // entre
																							// le
																							// joueur
																							// et
																							// le
																							// mur
																							// nord
								&& (posY_AI - joueurs[i].getTrace().get(j).getDebut().getY()) < distanceObstacle[0]) { // distance
																														// plus
																														// courte
																														// que
																														// la
																														// precedente
																														// en
																														// memoire
							distanceObstacle[0] = posY_AI - joueurs[i].getTrace().get(j).getDebut().getY();
						}
						// Sinon, l'obstacle le plus pres en direction nord est
						// le mur nord
						else if (distanceObstacle[0] > posY_AI) {
							distanceObstacle[0] = posY_AI;
						}
						// DIRECTION SUD
						// Si une trace est plus près du jour que du mur, c'est
						// la trace qui est pris en consideration comme prochain
						// obstacle
						if (joueurs[i].getTrace().get(j).getDebut().getX() == posX_AI // sur
																						// meme
																						// axe
																						// Nord-Sud
								&& joueurs[i].getTrace().get(j).getDebut().getY() > posY_AI // entre
																							// le
																							// joueur
																							// et
																							// le
																							// mur
																							// sud
								&& (joueurs[i].getTrace().get(j).getDebut().getY() - posY_AI) < distanceObstacle[1]) { // distance
																														// plus
																														// courte
																														// que
																														// la
																														// precedente
																														// en
																														// memoire
							distanceObstacle[1] = joueurs[i].getTrace().get(j).getDebut().getY() - posY_AI;
						}
						// Sinon, l'obstacle le plus pres en direction sud est
						// le mur sud
						else if (distanceObstacle[1] > dimensions.getY() - posY_AI) {
							distanceObstacle[1] = dimensions.getY() - posY_AI;
						}

						// DIRECTION OUEST
						// Si une trace est plus près du jour que du mur, c'est
						// la trace qui est pris en consideration comme prochain
						// obstacle
						if (joueurs[i].getTrace().get(j).getDebut().getY() == posY_AI // sur
																						// meme
																						// axe
																						// Est-Ouest
								&& joueurs[i].getTrace().get(j).getDebut().getX() < posX_AI // entre
																							// le
																							// joueur
																							// et
																							// le
																							// mur
																							// ouest
								&& (posX_AI - joueurs[i].getTrace().get(j).getDebut().getX()) < distanceObstacle[2]) { // distance
																														// plus
																														// courte
																														// que
																														// la
																														// precedente
																														// en
																														// memoire
							distanceObstacle[2] = posX_AI - joueurs[i].getTrace().get(j).getDebut().getX();
						}
						// Sinon, l'obstacle le plus pres en direction ouest est
						// le mur ouest
						else if (distanceObstacle[2] > posX_AI) {
							distanceObstacle[2] = posX_AI;
						}
						// DIRECTION EST
						// Si une trace est plus près du jour que du mur, c'est
						// la trace qui est pris en consideration comme prochain
						// obstacle
						if (joueurs[i].getTrace().get(j).getDebut().getY() == posY_AI // sur
																						// meme
																						// axe
																						// Est-Ouest
								&& joueurs[i].getTrace().get(j).getDebut().getX() > posX_AI // entre
																							// le
																							// joueur
																							// et
																							// le
																							// mur
																							// est
								&& (joueurs[i].getTrace().get(j).getDebut().getX() - posX_AI) < distanceObstacle[3]) { // distance
																														// plus
																														// courte
																														// que
																														// la
																														// precedente
																														// en
																														// memoire
							distanceObstacle[3] = joueurs[i].getTrace().get(j).getDebut().getX() - posX_AI;
						}
						// Sinon, l'obstacle le plus pres en direction est est
						// le mur est
						else if (distanceObstacle[3] > dimensions.getX() - posX_AI) {
							distanceObstacle[3] = dimensions.getX() - posX_AI;
						}
					}
				}
			}

			// Determine si je joueur ordinateur doit etre en mode defensif
			if (distanceObstacle[0] < distCritique || distanceObstacle[1] < distCritique
					|| distanceObstacle[2] < distCritique || distanceObstacle[3] < distCritique) {
				modeDefensif = true;
			}
			// Par contre, si le joueur ne fait que longer un mur, il peut
			// sortir du mode défensif pour passer a l'attaque (si il n'y a que
			// le mur comme obstacle)
			switch (joueurs[0].getDirection_courante()) {
			case 'N':
			case 'S':
				// à gauche du jeu (ok si rien a l'ouest)
				if ((posX_AI > (dimensions.getX() - distCritique)) && (distanceObstacle[2] > distCritique)) {
					modeDefensif = false;
				}
				// à droite du jeu (ok si rien a l'est)
				else if ((posX_AI < distCritique) && (distanceObstacle[3] > distCritique)) {
					modeDefensif = false;
				}
				break;

			case 'E':
			case 'O':
				// en bas du jeu (ok si rien au nord)
				if ((posY_AI > (dimensions.getY() - distCritique)) && (distanceObstacle[0] > distCritique)) {
					modeDefensif = false;
				}
				// en haut du jeu (ok si rien vers le sud)
				else if ((posY_AI < distCritique) && (distanceObstacle[1] > distCritique)) {
					modeDefensif = false;
				}
				break;
			}

			// Boucle qui cherche le joueur humain le plus pres. Cherche le plus
			// pres des joueurs humains et cible la tete
			double distancePlusProcheVoisin = 1000;
			Point cible = new Point(0, 0);
			for (int i = 1; i < 3; i++) {
				if (joueurs[i].isVivant()) {
					distanceJoueurs[i - 1] = Math.pow(Math.pow(joueurs[i].getTrace().tete().getX() - posX_AI, 2)
							+ Math.pow(joueurs[i].getTrace().tete().getY() - posY_AI, 2), 0.5);
					if (distanceJoueurs[i - 1] < distancePlusProcheVoisin) {
						distancePlusProcheVoisin = distanceJoueurs[i - 1];
						cible = joueurs[i].getTrace().tete();
					}
				}
			}

			// Si le joueur ordinateur n'est pas en mode defensif, il passe a
			// l'attaque et se lance sur le joueur humain le plus pres
			if (!modeDefensif) {
				switch (joueurs[0].getDirection_courante()) {
				case 'N': // Avec ordi en direction nord
					if (posY_AI - cible.getY() > cible.getX() - posX_AI
							&& posY_AI - cible.getY() > posX_AI - cible.getX()) {
						direction = 'N';
					}
					if (posX_AI - cible.getX() > posY_AI - cible.getY()
							&& posX_AI - cible.getX() > cible.getX() - posX_AI) {
						direction = 'O';
					}
					if (cible.getX() - posX_AI > posY_AI - cible.getY()
							&& cible.getX() - posX_AI > posX_AI - cible.getX()) {
						direction = 'E';
					}
					break;
				case 'S': // Avec ordi en direction sud
					if (cible.getY() - posY_AI > cible.getX() - posX_AI
							&& cible.getY() - posY_AI > posX_AI - cible.getX()) {
						direction = 'S';
					}
					if (posX_AI - cible.getX() > cible.getY() - posY_AI
							&& posX_AI - cible.getX() > cible.getX() - posX_AI) {
						direction = 'O';
					}
					if (cible.getX() - posX_AI > cible.getY() - posY_AI
							&& cible.getX() - posX_AI > posX_AI - cible.getX()) {
						direction = 'E';
					}
					break;

				case 'O': // Avec ordi en direction ouest
					if (posX_AI - cible.getX() > cible.getY() - posY_AI
							&& posX_AI - cible.getX() > posY_AI - cible.getY()) {
						direction = 'O';
					}
					if (posY_AI - cible.getY() > posX_AI - cible.getX()
							&& posY_AI - cible.getY() > cible.getY() - posY_AI) {
						direction = 'N';
					}
					if (cible.getY() - posY_AI > posX_AI - cible.getX()
							&& cible.getY() - posY_AI > posY_AI - cible.getY()) {
						direction = 'S';
					}
					break;

				case 'E': // Avec ordi en direction Est
					if (cible.getX() - posX_AI > cible.getY() - posY_AI
							&& cible.getX() - posX_AI > posY_AI - cible.getY()) {
						direction = 'E';
					}
					if (posY_AI - cible.getY() > cible.getX() - posX_AI
							&& posY_AI - cible.getY() > cible.getY() - posY_AI) {
						direction = 'N';
					}
					if (cible.getY() - posY_AI > cible.getX() - posX_AI
							&& cible.getY() - posY_AI > posY_AI - cible.getY()) {
						direction = 'S';
					}
					break;
				}
			}
			// Prise de décision en mode defensif
			else {
				if (directionAI == 'N' && distanceObstacle[0] <= distCritique) {
					if (distanceObstacle[2] >= distanceObstacle[3]) {
						direction = 'O';
					} else {
						direction = 'E';
					}
				} else if (directionAI == 'S' && distanceObstacle[1] <= distCritique) {
					if (distanceObstacle[2] >= distanceObstacle[3]) {
						direction = 'O';
					} else {
						direction = 'E';
					}
				} else if (directionAI == 'O' && distanceObstacle[2] <= distCritique) {
					if (distanceObstacle[0] >= distanceObstacle[1]) {
						direction = 'N';
					} else {
						direction = 'S';
					}
				} else if (directionAI == 'E' && distanceObstacle[3] <= distCritique) {
					if (distanceObstacle[0] >= distanceObstacle[1]) {
						direction = 'N';
					} else {
						direction = 'S';
					}
				}
			}
		}
		timerAction--;
		return direction;
	}
}
