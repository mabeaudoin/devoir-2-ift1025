import java.awt.Color;

/**
 * Constructeur de JoueurHumain, , extension de la classe abstraite
 * {@link Joueur}.
 * 
 * @author Marc-Andre Beaudoin et Etienne Boucher
 *
 */
public class JoueurHumain extends Joueur {

	/**
	 * Constructeur du JoueurHumain (lorsque le JoueurHumain est inclu dans la
	 * partie)
	 * 
	 * @param @param
	 *            tete premier segment de la trace
	 * @param direction
	 *            direction initiale
	 * @param couleur
	 *            couleur du trait du JoueurHumain
	 */
	public JoueurHumain(Segment tete, char direction, Color couleur) {
		trace = new Trace(tete);
		direction_courante = direction;
		nouvelle_direction = direction;
		this.couleur = couleur;
		this.vivant = true;
		this.inGame = true;	
	}

	/**
	 * Constructeur du JoueurHumain (lorsque le JoueurHumain n'est pas inclu
	 * dans la partie)
	 * 
	 */
	public JoueurHumain() {
		trace = new Trace();
		direction_courante = '\u0000';
		this.couleur = null;
		this.vivant = false;
		this.inGame = false;	
	}
}
