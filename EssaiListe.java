/**
 * classe EssaiListe qui sert a faire l'essai des ListeTableau et ListeChainee
 * 
 * @author Etienne Boucher et Marc-André Beaudoin
 *
 */
public class EssaiListe {
	/**
	 * methode main qui execute un test en fonction des arguments
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int chainee = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		Liste liste;

		// Crée le type de liste voulu en fonction des arguments
		if (chainee == 0) {
			liste = new ListeTableau();
		} else if (chainee == 1) {
			liste = new ListeChainee();
		} else {
			throw new IllegalArgumentException("Premier argument invalide (doit être 0 ou 1)");
		}
		// Lance testListe()
		EssaiListe.testListe(liste, n);

	}

	/**
	 * methode de test des ListeChainee et ListeTableau
	 * 
	 * @param l
	 *            listeTableau ou listeChainee
	 * @param n
	 *            nombre d'entree
	 */
	public static void testListe(Liste l, int n) {
		// append n objets dans la liste
		System.out.println("append...");
		for (int i = 1; i <= n; i++) {
			l.append(new Integer(i));
		}
		if (l.size() <= 100) {
			System.out.println(l);
		}
		// preprend n objets dans la liste
		System.out.println("preprend...");
		for (int i = 1; i <= n; i++) {
			l.preprend(new Integer(i));
		}
		if (l.size() <= 100) {
			System.out.println(l);
		}

		// remove first, enlève les entrées pour l'équivalent de la moitié de n
		System.out.println("removeFirst...");
		for (int i = 0; i < (n / 2); i++) {
			l.removeFirst();
		}
		System.out.println(l.removeFirst());

		// impression de la liste si elle a moins de 100 entrées.
		if (l.size() <= 100) {
			System.out.println(l);
		}
		// change avant-dernier pour "remplace"
		l.set(l.size() - 2, "Remplace");
		System.out.println("taille=" + l.size() + "  premier=" + l.getFirst() + "  dernier=" + l.getLast());

		// Impression de l'avant dernier si la liste a moins de 100 entrées
		System.out.println("avant-dernier=" + l.get(l.size() - 2));

		// impression finale de la liste
		if (l.size() <= 100) {
			System.out.println(l);
		}
	}
}