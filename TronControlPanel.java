import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;

import javax.swing.*;

/**
 * sous-classe de {@link JPanel} qui cree un JPanel contenant les elements de
 * controle du jeu de la classe {@link Tron}.
 *
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class TronControlPanel extends JPanel {

	// variables de la classe
	private String[] choiceList = { "1 joueur, 1 automate", "2 joueurs, 1 automate", "2 joueurs, sans automate" };
	private int playerChoice = 0;
	protected Joueur[] joueurs;

	private static final long serialVersionUID = 1L;
	private JButton startButton = new JButton("START");
	private JButton pauseButton = new JButton("PLAY");
	private JTextField speedField = new JTextField("50");
	private JButton speedUpButton = new JButton("\u2191");
	private JButton speedDownButton = new JButton("\u2193");
	private JComboBox<String> comboBox = new JComboBox<String>(choiceList);

	private int[] score = new int[3];
	private int timerDelay = 50;
	private Point dimensions;
	private TronColorPanel colorPanel;
	private TronScorePanel scorePanel;
	private Timer timer;
	private JLabel messageBox;
	private int nbJoueursVivants;

	/**
	 * Constructeur de TronPanel
	 */
	public TronControlPanel(Point dimensions, final Joueur[] joueurs, final TronColorPanel colorPanel,
			TronScorePanel scorePanel) {
		// Mise en place du TronControlPanel
		setLayout(new BorderLayout());
		setOpaque(true);
		setBackground(Color.BLACK);
		setFocusable(true);

		// initialisation d'une "connection par référence" avec les autres
		// panels
		this.dimensions = dimensions;
		this.joueurs = joueurs;
		this.colorPanel = colorPanel;
		this.scorePanel = scorePanel;

		// Creation du timer qui lance la boucle de jeu
		timer = new Timer(timerDelay, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});

		// Ajout de l'interactivité du bouton START
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (startButton.getText().equals("START")) {
					startup();
					messageBox.setText("Appuyer sur PLAY pour débuter la partie");
					startButton.setText("NEW GAME");
				} else {
					newGameCheck();
				}
			}
		});

		// Ajout de l'interactivité du bouton PAUSE
		pauseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (startButton.getText().equals("NEW GAME") && pauseButton.getText().equals("PLAY")) {
					timer.start();
					messageBox.setText(" ");
					pauseButton.setText("PAUSE");
				} else if (pauseButton.getText().equals("PROCHAINE MANCHE")) {
					startup();
					messageBox.setText("Appuyer sur PLAY pour débuter la partie");
					pauseButton.setText("PLAY");
				} else if (startButton.getText().equals("NEW GAME")) {
					timer.stop();
					messageBox.setText("Pause");
					pauseButton.setText("PLAY");
				}
			}
		});

		// Ajout de l'interactivité à la comboBox de selection de joueurs
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerChoice = comboBox.getSelectedIndex();
			}
		});

		// Ajout de l'interactivité des comboBoxs de choix de couleur
		for (int i = 0; i < joueurs.length; i++) {
			final int k = i;
			final JComboBox<String> colorBox = colorPanel.getComboBoxsJoueurs()[i].getColorBox();
			colorBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					joueurs[k].setCouleur(colorPanel.getColor(k));
				}
			});
		}

		// Ajout de l'interactivite avec le JTextField et les boutons +/1 qui
		// fait un update de la vitesse du timer
		speedField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timerDelay = Integer.parseInt(speedField.getText());
				if (timerDelay < 5) {
					timerDelay = 5;
				}
				speedField.setText("" + timerDelay);
				timer.setDelay(timerDelay);
			}
		});
		speedUpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.stop();
				messageBox.setText("Pause");
				pauseButton.setText("PLAY");
				timerDelay += 5;
				speedField.setText("" + timerDelay);
				timer.setDelay(timerDelay);
			}
		});
		speedDownButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.stop();
				messageBox.setText("Pause");
				pauseButton.setText("PLAY");
				if (timerDelay >= 10)
					timerDelay -= 5;
				speedField.setText("" + timerDelay);
				timer.setDelay(timerDelay);
			}
		});

		// Timer panel pour les elements d<ajustement du timer
		JPanel timerPanel = new JPanel();
		timerPanel.setLayout(new BorderLayout());
		timerPanel.setPreferredSize(new Dimension(140, 35));
		speedField.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel speedLabel = new JLabel("vitesse du jeu (ms)");
		speedLabel.setHorizontalAlignment(JLabel.CENTER);
		timerPanel.add(speedLabel, BorderLayout.NORTH);
		timerPanel.add(speedUpButton, BorderLayout.WEST);
		timerPanel.add(speedField, BorderLayout.CENTER);
		timerPanel.add(speedDownButton, BorderLayout.EAST);

		// Ajout des differents elements à l'objet TronControlPanel
		JPanel tempPanel1 = new JPanel();
		tempPanel1.setOpaque(true);
		tempPanel1.setBackground(Color.BLACK);
		tempPanel1.add(startButton);
		tempPanel1.add(pauseButton);
		tempPanel1.add(comboBox);
		tempPanel1.add(timerPanel);

		// Ajout de l'image tronlogo dans un panel temporaire
		JPanel tempPanel2 = new JPanel();
		tempPanel2.setBackground(Color.BLACK);
		tempPanel2.add(new JLabel(new ImageIcon("./tronlogo.jpg")));

		// Ajout d'un texte d'état du jeu
		JPanel messagePanel = new JPanel();
		messagePanel.setBackground(Color.black);
		messageBox = new JLabel("I fight for the user! - Tron", JLabel.CENTER);
		Font font = new Font("Verdana", Font.ITALIC, 12);
		messageBox.setForeground(Color.white);
		messageBox.setFont(font);
		messagePanel.add(messageBox);

		// Integration des panels temporaires au TronControlPanel
		add(tempPanel2, BorderLayout.NORTH);
		add(tempPanel1, BorderLayout.CENTER);
		add(messagePanel, BorderLayout.SOUTH);

		// Initialisation du keyListener pour le tronPanel
		initialize_keyListener();
	}

	/**
	 * methode qui cree un KeyListener dans une classe anonyme. Les reactions
	 * aux touches y sont decrites pour les deux joueurs. Les mouvements sont
	 * restreints a des tournant de 90 degres. Le joueur ne peut faire
	 * demi-tour.
	 *
	 */
	public void initialize_keyListener() {

		this.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				char direction = Character.toUpperCase(e.getKeyChar());
				switch (direction) {
				// Touches pour le joueur 1
				case 'W':
					if (joueurs[1].getDirection_courante() == 'O' || joueurs[1].getDirection_courante() == 'E') {
						joueurs[1].setNouvelle_direction('N');
					}
					break;
				case 'S':
					if (joueurs[1].getDirection_courante() == 'O' || joueurs[1].getDirection_courante() == 'E') {
						joueurs[1].setNouvelle_direction('S');
					}
					break;
				case 'A':
					if (joueurs[1].getDirection_courante() == 'N' || joueurs[1].getDirection_courante() == 'S') {
						joueurs[1].setNouvelle_direction('O');
					}
					break;
				case 'D':
					if (joueurs[1].getDirection_courante() == 'N' || joueurs[1].getDirection_courante() == 'S') {
						joueurs[1].setNouvelle_direction('E');
					}
					break;
				// Touches pour le joueur 2
				case 'I':
					if (joueurs[2].getDirection_courante() == 'O' || joueurs[2].getDirection_courante() == 'E') {
						joueurs[2].setNouvelle_direction('N');
					}
					break;
				case 'M':
					if (joueurs[2].getDirection_courante() == 'O' || joueurs[2].getDirection_courante() == 'E') {
						joueurs[2].setNouvelle_direction('S');
					}
					break;
				case 'J':
					if (joueurs[2].getDirection_courante() == 'N' || joueurs[2].getDirection_courante() == 'S') {
						joueurs[2].setNouvelle_direction('O');
					}
					break;
				case 'K':
					if (joueurs[2].getDirection_courante() == 'N' || joueurs[2].getDirection_courante() == 'S') {
						joueurs[2].setNouvelle_direction('E');
					}
					break;
				}
			}
		});
	}

	/**
	 * methode qui initialise les parametres en debut de partie ou lorsqu'une
	 * nouvelle partie est declanchee
	 *
	 */
	private void startup() {
		requestFocus(true);
		messageBox.setText(" ");

		// Créer chacun des joueurs
		if (playerChoice == 0 || playerChoice == 1) {
			Point p1 = randCoord(dimensions);
			Point p2 = randDist1(dimensions, p1);
			joueurs[0] = new JoueurOrdinateur(new Segment(p1, p2), direction(p1, p2), colorPanel.getColor(0));
		} else {
			joueurs[0] = new JoueurOrdinateur();
		}
		if (playerChoice == 0 || playerChoice == 1) {
			Point p1 = randCoord(dimensions);
			while (joueurs[0].getTrace().contient(p1)) {
			p1 = randCoord(dimensions);
			}
			Point p2 = randDist1(dimensions, p1);
			while (joueurs[0].getTrace().contient(p2)) {
			p2 = randDist1(dimensions, p1);
			}
			joueurs[1] = new JoueurHumain(new Segment(p1, p2), direction(p1, p2), colorPanel.getColor(1));
		} else {
			Point p1 = randCoord(dimensions);
			Point p2 = randDist1(dimensions, p1);
			joueurs[1] = new JoueurHumain(new Segment(p1, p2), direction(p1, p2), colorPanel.getColor(1));
		}
		if (playerChoice == 1) {
			Point p1 = randCoord(dimensions);
			while (joueurs[0].getTrace().contient(p1) ||
			joueurs[1].getTrace().contient(p1)) {
			p1 = randCoord(dimensions);
			}
			Point p2 = randDist1(dimensions, p1);
			while (joueurs[0].getTrace().contient(p2) ||
			joueurs[1].getTrace().contient(p2)) {
			p2 = randDist1(dimensions, p1);
			}
			joueurs[2] = new JoueurHumain(new Segment(p1, p2), direction(p1, p2), colorPanel.getColor(2));
		} else if (playerChoice == 2) {
			Point p1 = randCoord(dimensions);
			while (joueurs[1].getTrace().contient(p1)) {
			p1 = randCoord(dimensions);
			}
			Point p2 = randDist1(dimensions, p1);
			while (joueurs[1].getTrace().contient(p2)) {
			p2 = randDist1(dimensions, p1);
			}
			joueurs[2] = new JoueurHumain(new Segment(p1, p2), direction(p1, p2), colorPanel.getColor(2));
		} else {
			joueurs[2] = new JoueurHumain();
		}
		draw();
	}

	/**
	 * Methode qui actualise les parametres du jeu a chaque iteration du timer,
	 * fait bouger les joueurs et affiche les scores. Detecte egalement des
	 * conditions de victoire et le pointage des joueurs (+1 pour chaque joueurs
	 * qui entre dans sa trace et +1 pour le dernier joueur restant)
	 *
	 */
	private void update() {
		// remet le focus dans le TronControlPanel
		requestFocus(true);

		// Actualise les scores
		scorePanel.setScores(score);

		// Détermination de la prochaine direction du joueur ordinateur
		if (joueurs[0].isVivant()) {
			nextMoveAI();
		}

		// Boucle qui redefinie les direction et détecte les collisions
		for (int i = 0; i < 3 && timer.isRunning(); i++) {
			if (joueurs[i].isVivant()) {
				// Changement de direction et allonge la trace
				if (joueurs[i].getDirection_courante() != joueurs[i].getNouvelle_direction()) {
					joueurs[i].setDirection_courante(joueurs[i].getNouvelle_direction());
				}

				// Determination de la future position de la tete
				Point futureTete = new Point(joueurs[i].getTrace().tete().getX(), joueurs[i].getTrace().tete().getY());
				switch (joueurs[i].getDirection_courante()) {
				case 'N':
					futureTete.setY(futureTete.getY() - 1);
					break;
				case 'S':
					futureTete.setY(futureTete.getY() + 1);
					break;
				case 'E':
					futureTete.setX(futureTete.getX() + 1);
					break;
				case 'O':
					futureTete.setX(futureTete.getX() - 1);
					break;
				}

				// Detection de collision avec un mur
				if (futureTete.getX() == 0 || futureTete.getY() == 0 || futureTete.getX() == dimensions.getX()
						|| futureTete.getY() == dimensions.getY()) {
					joueurs[i].setVivant(false);
					// Message pour s'être applati dans le mur
					if (i == 0) {
						messageBox.setText("L'ordinateur s'est applati dans le mur!");
					} else {
						messageBox.setText("Le joueur " + i + " s'est applati sur le mur!");
					}
				}

				// Detection des collisions avec autres joueurs(regarde la
				// trace si le joueur etait dans le jeu (direction pas
				// \u0000)
				for (int j = 0; j < 3; j++) {
					if (joueurs[j].getDirection_courante() != '\u0000') {
						if (joueurs[j].getTrace().contient(futureTete)) {
							// Joueur mort! Un point pour le joueur qui a
							// bloqué (si ce n'est pas un suicide,
							// évidament!)
							joueurs[i].setVivant(false);
							if (i != j) {
								// Message kaput
								if (j == 0) {
									messageBox.setText(
											"Le joueur " + i + " s'est écrabouillé dans la trace de l'ordinateur");
								} else if (i == 0) {
									messageBox.setText("L'ordinateur s'est écrabouillé dans la trace du joueur " + j);
								} else {
									messageBox.setText(
											"Le joueur " + i + " s'est écrabouillé dans la trace du joueur " + j);
								}
								;
							}
							// Message de suicides
							else {
								if (i == 0) {
									messageBox.setText("L'ordinateur s'est suicidé");
								} else {
									messageBox.setText("Le joueur " + i + " s'est suicidé!");
								}
							}
						}
					}
				}
			}
			// Allonge la trace du joueur si encore vivant
			if (joueurs[i].isVivant())
				joueurs[i].getTrace().allonge(joueurs[i].getDirection_courante());

			// }

			// Test et message de victoire lorsqu'il ne reste qu'un seul joueur.
			// Provoque l'arret du timer
			nbJoueursVivants = 0;
			for (int k = 0; k < 3; k++) {
				if (joueurs[k].isVivant() == true)
					nbJoueursVivants++;
			}
			if (nbJoueursVivants == 1) {
				if (joueurs[0].isVivant()) {
					messageBox.setText("L'ordinateur a gagné la partie");
					score[0]++;
				}
				if (joueurs[1].isVivant()) {
					messageBox.setText("Le joueur 1 a gagné la partie");
					score[1]++;
				}
				if (joueurs[2].isVivant()) {
					messageBox.setText("Le joueur 2 a gagné la partie");
					score[2]++;
				}
				scorePanel.setScores(score);
				timer.stop();
				pauseButton.setText("PROCHAINE MANCHE");
			}
		}
		draw();
	}

	/**
	 * methode qui reactualise l'affichage du paneau de jeu
	 *
	 */
	private void draw() {
		getParent().revalidate();
		getParent().repaint();
	}

	/**
	 * methode qui fait apparaitre) une fenetre qui demande a l'utilisateur s'il
	 * veut recommencer la partie
	 *
	 */
	public void newGameCheck() {
		// pause le jeu le temps du pop-up
		timer.stop();
		// Outil pour centrer la fenetre qui pop
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		// Composantes de l'affichage
		final JFrame checkFrame = new JFrame();
		final JPanel checkPanel = new JPanel();
		final JLabel label = new JLabel("Nouvelle partie?");
		final JButton yesButton = new JButton("OUI");
		final JButton noButton = new JButton("NON");

		// Listeners, boutons, frame, panels et frames sont emboites
		yesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pauseButton.setText("PLAY");
				checkFrame.dispose();
				score = new int[3];
				scorePanel.setScores(score);
				startup();
			}
		});
		noButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.start();
				checkFrame.dispose();
			}
		});
		checkPanel.add(label);
		checkPanel.add(yesButton);
		checkPanel.add(noButton);
		checkFrame.add(checkPanel);
		checkFrame.pack();
		checkFrame.setVisible(true);

		// Centrage du pop-up dans l'ecran
		checkFrame.setLocation((dimension.width - checkFrame.getWidth()) / 2,
				(dimension.height - checkFrame.getHeight()) / 2);
	}

	/**
	 * Fonction qui génère le prochaine mouvement de l'ordinateur. Fait appel
	 * aux fonctions statiques de JoueurOrdinateur pour la prise de décision
	 *
	 */
	public void nextMoveAI() {

		char alarmeCollision = JoueurOrdinateur.computerAI(joueurs, dimensions, joueurs[0].getDirection_courante());
		if (alarmeCollision != '\u0000') {
			joueurs[0].setNouvelle_direction(alarmeCollision);
		}

	}

	/**
	 * methode qui retourne le choix de joueurs dans le jeu
	 *
	 * @return le choix des joueurs
	 */
	public int getPlayerChoice() {
		return playerChoice;
	}

	/**
	 * Methode qui retourne un Point aleatoirement situe a l'interieur des dimensions fournies en parametre
	 * @param dimensions dimensions a l'interieur desquelles le Point doit se situr
	 * @return le Point
	 */
	public Point randCoord(Point dimensions) {
		int x = (int) (dimensions.getX() * 0.7 * Math.random() + dimensions.getX() * 0.15);
		int y = (int) (dimensions.getY() * 0.7 * Math.random() + dimensions.getY() * 0.15);
		return new Point(x, y);
	}

	/**
	 * Methode qui retourne un Point a une distance de 1 unite du Point de reference
	 * @param dimensions dimensions a l'interieur desquelles le Point doit se situer
	 * @param reference Point a cote duquel on doit creer un second Point
	 * @return un Point a une distance de 1 unite du Point de reference
	 */
	public Point randDist1(Point dimensions, Point reference) {
		Point p = new Point(-1, -1);
		while (p.getX() < 0 || p.getX() >= dimensions.getX() || p.getY() < 0 || p.getY() >= dimensions.getY()) {
			switch ((int) (Math.random() * 4)) { // arrondi à l'entier près
			case 0: // vers le haut
				p = new Point(reference.getX(), reference.getY() - 1);
				break;
			case 1: // vers le gauche
				p = new Point(reference.getX() - 1, reference.getY());
				break;
			case 2: // vers le bas
				p = new Point(reference.getX(), reference.getY() + 1);
				break;
			case 3: // vers le droite
				p = new Point(reference.getX() + 1, reference.getY());
				break;
			}
		}
		return p;
	}

	/**
	 * Fonction qui determine la direction dans laquelle se dirige un Segment entre des Points p1 et p2
	 * @param p1 Point de tete
	 * @param p2 Point de queue
	 * @return la direction dans laquelle se dirige un Segment entre des Points p1 et p2
	 */
	public static char direction(Point p1, Point p2) {
		if (p1.getX() - p2.getX() == 0) {
			if (p1.getY() - p2.getY() > 0) {
				return 'S';
			} else {
				return 'N';
			}
		} else {
			if (p1.getX() - p2.getX() > 0) {
				return 'E';
			} else {
				return 'O';
			}
		}
	}
}
