import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

/** Classe qui cree le panneau du bas de l'ecran de jeu qui indique les controles au joueurs
 *
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class TronBottomBarPanel extends JPanel {

	//Variables du JPanel
	JWindow ctrlWindow = new JWindow();
	private static final long serialVersionUID = 1L;

	/** constructeur de TronBottomBarPanel
	 *
	 */
	public TronBottomBarPanel() {
	    //Mise en place du du TronBottomBarPanel
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		setPreferredSize(new Dimension(100,35));
		setLayout(new BorderLayout());
		setOpaque(true);
		setBackground(Color.BLACK);

		//Creation du bouton qui fait un pop-up avec les contrôles du jeu
		final JButton controles = new JButton("Contrôles");
		controles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JPanel ctrlPanel = new JPanel();
				JButton ctrlButton = new JButton("OK");
				ctrlButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ctrlWindow.dispose();
					}
				});

				//Mise en forme du pop-up dans une grille et utilisant des JPanel secondaires pour les colonnes
				ctrlPanel.setBorder(BorderFactory.createLineBorder(Color.black));
				ctrlPanel.setLayout(new GridLayout(1,5));

				JPanel playerBar = new JPanel();
				playerBar.setLayout(new GridLayout(3,1));
				playerBar.add(new JLabel(" ", JLabel.CENTER));
				playerBar.add(new JLabel("Joueur 1 ", JLabel.CENTER));
				playerBar.add(new JLabel("Joueur 2 ", JLabel.CENTER));

				JPanel northBar = new JPanel();
				northBar.setBackground(Color.lightGray);
				northBar.setLayout(new GridLayout(3,1));
				northBar.add(new JLabel("NORD", JLabel.CENTER));
				northBar.add(new JLabel("W", JLabel.CENTER));

				northBar.add(new JLabel("I", JLabel.CENTER));

				JPanel southBar = new JPanel();
				southBar.setLayout(new GridLayout(3,1));
				southBar.add(new JLabel("SUD", JLabel.CENTER));
				southBar.add(new JLabel("S", JLabel.CENTER));
				southBar.add(new JLabel("K", JLabel.CENTER));

				JPanel eastBar = new JPanel();
				eastBar.setBackground(Color.lightGray);
				eastBar.setLayout(new GridLayout(3,1));
				eastBar.add(new JLabel("EST", JLabel.CENTER));
				eastBar.add(new JLabel("D", JLabel.CENTER));
				eastBar.add(new JLabel("L", JLabel.CENTER));

				JPanel westBar = new JPanel();
				westBar.setLayout(new GridLayout(3,1));
				westBar.add(new JLabel("OUEST", JLabel.CENTER));
				westBar.add(new JLabel("A", JLabel.CENTER));
				westBar.add(new JLabel("J", JLabel.CENTER));

				ctrlPanel.add(playerBar);
				ctrlPanel.add(northBar);
				ctrlPanel.add(southBar);
				ctrlPanel.add(eastBar);
				ctrlPanel.add(westBar);

				//Creation d'un JPanel temporaire pour le bouton et les indications
				JPanel tempPanel = new JPanel();
				tempPanel.setLayout(new BorderLayout());
				tempPanel.add(ctrlPanel, BorderLayout.CENTER);
				tempPanel.add(ctrlButton, BorderLayout.SOUTH);

				//Setup du ctrlWindow
				ctrlWindow.add(tempPanel);
				ctrlWindow.setLocationRelativeTo(controles);
				ctrlWindow.pack();
				ctrlWindow.setVisible(true);
			}
		});

		//Ajout du bouton d'affichage des contrôles
		add(controles, BorderLayout.EAST);
	}
}
