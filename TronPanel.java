import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 * Element principal du jeu tron. Inclue tout les autres JPanels du jeu et
 * l'arene.
 * 
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class TronPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	// Variables de jeu
	Joueur[] joueurs = { new JoueurOrdinateur(), new JoueurHumain(), new JoueurHumain() };

	// Composantes principales
	private Arene arena;
	private TronControlPanel controlPanel;
	private TronColorPanel colorPanel;
	private TronScorePanel scorePanel;

	/**
	 * constructeur du TronPanel
	 * 
	 * @param largeur
	 *            largeur de la zone de jeu
	 * @param hauteur
	 *            hauteur de la zone de jeu
	 */
	public TronPanel(int largeur, int hauteur) {
		arena = new Arene(largeur, hauteur, joueurs);
		colorPanel = new TronColorPanel(3);
		scorePanel = new TronScorePanel();
		Point dimensions = new Point(largeur, hauteur); // Point en bas à droite
		controlPanel = new TronControlPanel(dimensions, joueurs, colorPanel, scorePanel);

		// Parametres du TronPanel
		setLayout(new BorderLayout());

		// Ajout des elements au TronPanel
		add(controlPanel, BorderLayout.NORTH);
		add(colorPanel, BorderLayout.WEST);
		add(scorePanel, BorderLayout.EAST);
		add(arena, BorderLayout.CENTER);
		add(new TronBottomBarPanel(), BorderLayout.SOUTH);
	}
}