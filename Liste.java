/** Interface pour les classe implementant des listes chainées
 *  
 * @author Etienne Boucher et Marc-Andre Beaudoin
 *
 */
public interface Liste
{

	/**retourne le nombre d'éléments dans la liste
	 * 
	 * @return la taille de la liste chainée
	 */
    abstract int size();
    
    /**  ajoute l'objet o à la fin de la liste (la taille de la liste augmente donc de 1)
     * 
     * @param o objet à insérer à la fin de la liste
     */
    abstract void append(Object o);
    
    /** insère l'objet o au début de la liste (la taille de la liste augmente donc de 1)
     * 
     * @param o objet à insérer au début de la liste
     */
    abstract void preprend(Object o);
    
    /** retourne une référence au premier objet de la liste (sans l'enlever de la liste)
     * 
     * @return référence au premier objet de la liste
     */
    abstract Object getFirst();
    
    /** retourne une référence au dernier objet de la liste (sans l'enlever de la liste)
     * 
     * @return référence au dernier objet de la liste
     */
    abstract Object getLast();
    
    /** retourne le premier objet de la liste après l'avoir enlevé de la liste (la taille de la liste diminue donc de 1)
     * 
     * @return le premier objet de la liste 
     */
    abstract Object removeFirst(); 
    
    /** retourne une référence à l'objet stocké à la position donnée (entre 0 et size()-1)
     * 
     * @param position position dans la liste chainée
     * @return la reference a l'objet en position donnée
     */
    abstract Object get(int position);
    
    /** remplace par o l'objet à la position donnée (entre 0 et size()-1) (précisément on remplace la référence à cette position par o)
     * 
     * @param position position dans la liste chainée pour insérer l'objet
     * @param o objet a placer dans la liste chainée pour remplacer l'objet
     */
    abstract void set(int position, Object o); 

}