import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Classe qui est forme d'un JPanel contenant un ComboBox qui permet de choisir
 * la couleur des joueurs
 * 
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class ColorComboBox extends JPanel {

	// Variables de classe
	private Color color;
	private String[] colorList = { "Bleu", "Vert", "Rouge", "Orange", "Jaune" };
	private JComboBox<String> colorBox = new JComboBox<String>(colorList);
	private static final long serialVersionUID = 1L;

	/**
	 * constructeur d'objets ColorComboBox qui permettent de choisir la couleur
	 * des joueurs
	 * 
	 * @param color
	 *            soit les index 0(Bleu), 1(Vert), 2(Rouge), 3(Orange) ou
	 *            4(Jaune)
	 */
	public ColorComboBox(int colorCode) {
		setOpaque(true);

		switch (colorCode) {
		case 0:
			color = new Color(0, 0, 255);
			colorBox.setSelectedIndex(colorCode);
			setBackground(color);
			break;
		case 1:
			color = new Color(0, 255, 0);
			colorBox.setSelectedIndex(colorCode);
			setBackground(color);
			break;
		case 2:
			color = new Color(255, 0, 0);
			colorBox.setSelectedIndex(colorCode);
			setBackground(color);
			break;
		case 3:
			color = new Color(255, 128, 0);
			colorBox.setSelectedIndex(colorCode);
			setBackground(color);
			break;
		case 4:
			color = new Color(255, 255, 0);
			colorBox.setSelectedIndex(colorCode);
			setBackground(color);
			break;
		default:
			setBackground(Color.BLACK);
			throw new IllegalArgumentException("Couleur non reconnue : " + color);
		}

		colorBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// colorIndex = colorBox.getSelectedIndex();
				// colorBox.setSelectedIndex(colorIndex);
				switch (colorBox.getSelectedIndex()) {
				case 0:
					color = new Color(0, 0, 255);
					setBackground(color);
					break;
				case 1:
					color = new Color(0, 255, 0);
					setBackground(color);
					break;
				case 2:
					color = new Color(255, 0, 0);
					setBackground(color);
					break;
				case 3:
					color = new Color(255, 128, 0);
					setBackground(color);
					break;
				case 4:
					color = new Color(255, 255, 0);
					setBackground(color);
					break;
				}
			}
		});
		this.add(colorBox);
	}

	/**
	 * methode getColor() qui retourne un objet de type Color correspondant à la
	 * couleur défini dans le ColorComboBox
	 * 
	 * @return Color représentant la couleur choisie dans le ColorComboBox
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return colorBox, la comboBox
	 */
	public JComboBox<String> getColorBox() {
		return colorBox;
	}
}