/**
 * classe représentant une liste dans un tableau d'objets. Contient les méthodes
 * décrites dans l'interface Liste
 * 
 * @author Etienne Boucher et Marc-André Beaudoin
 *
 */
public class ListeTableau implements Liste {
	private int n; // nombre d'objets dans le tableau
	private Object[] tab; // tableau qui contient les objets de la liste

	/**
	 * Constructeur par défaut qui créé une liste chainée de taille 0 avec un
	 * tableau d'objet de taille 10
	 * 
	 */
	public ListeTableau() {
		this.n = 0;
		this.tab = new Object[10];
	}

	@Override
	public int size() {
		return this.n;
	}

	@Override
	public void append(Object o) {
		// verification de la taille du tableau, envois a la methode stretchTab
		// si la taille est insuffisante
		if (n == tab.length) {
			stretchTab();
		}

		this.tab[n] = o;
		n++;
	}

	@Override
	public void preprend(Object o) {
		// verification de la taille du tableau, envois a la methode stretchTab
		// si la taille est insuffisante
		if (n == tab.length) {
			stretchTab();
		}
		// recopie les elements du tableau en avant d'une case en avant
		for (int i = n; i > 0; i--) {
			tab[i] = tab[i - 1];
		}
		// crée un noeud avec l'objet passé en parametre et le met en position 0
		// du tableau tab puis incrémente n
		tab[0] = o;
		n++;
	}

	@Override
	public Object getFirst() {
		return tab[0];
	}

	@Override
	public Object getLast() {
		return tab[n - 1];
	}

	@Override
	public Object removeFirst() {
		// Enregistre temporairement l'objet en position 0
		Object temp = tab[0];
		// Tasse toutes les references de une position vers les plus petit
		// indices
		for (int i = 0; i < n - 1; i++) {
			tab[i] = tab[i + 1];
		}
		// elimine l'objet en bout de tableau et décrémente
		tab[n - 1] = null;
		n--;

		// si le tableau contient plus de 10 cases vides, le tableau la methode
		// shrinkTab() est appelée pour en réduire la taille
		if (n <= tab.length - 10) {
			shrinkTab();
		}

		// retourne le prenier noeud qui etant en position 0 dans tab avant
		// reorganisation
		return temp;

	}

	@Override
	public Object get(int position) {
		// Si dépasse l'étendue du tableau, lance une erreur
		if (position < 0 || position >= n) {
			throw new ArrayIndexOutOfBoundsException("position hors du tableau");
		}
		// Sinon, retourne l'objet
		return tab[position];
	}

	@Override
	public void set(int position, Object o) {
		// Si dépasse l'étendue du tableau, lance une erreur
		if (position < 0 || position >= n) {
			throw new ArrayIndexOutOfBoundsException("position hors du tableau");
		}
		// Sinon, met l'objet dans la bonne position
		tab[position] = o;
	}

	/**
	 * methode toSting() qui override la methode de la classe Object. Retourne
	 * un String qui représente la liste dans le tableau.
	 * 
	 * @return un String qui représente la liste dans le tableau
	 */
	public String toString() {
		String ligne = "ListeTableau[ ";
		// Ajout du toString du premier objet sans virgule
		ligne = ligne + tab[0];
		// Boucle pour les autres
		for (int i = 1; i < n; i++) {
			ligne = ligne + ", " + tab[i];

		}
		ligne = ligne + " ]";

		return ligne;
	}

	/**
	 * methode qui ajoute 10 espaces dans le tableau tab de l'objet ListeTableau
	 * 
	 */
	private void stretchTab() {
		int length = tab.length;

		// cree un nouveau tableau d'une taille supérieure de 10
		Object[] newTab = new Object[length + 10];
		// Copie les références inscrites dans le tableau original vers le
		// nouveau tableau
		for (int i = 0; i < length; i++) {
			newTab[i] = tab[i];
		}
		// change le pointeur du tableau pour celui du nouveau tableau d'une
		// taille supérieure de 10
		tab = newTab;
	}

	/**
	 * methode qui retire 10 espaces dans le tableau tab de l'objet ListeTableau
	 * 
	 */
	private void shrinkTab() {
		int length = tab.length;

		// cree un nouveau tableau d'une taille inférieure de 10
		Object[] newTab = new Object[length - 10];
		// Copie les références inscrites dans le tableau original vers le
		// nouveau tableau
		for (int i = 0; i < length - 10; i++) {
			newTab[i] = tab[i];
		}
		// change le pointeur du tableau pour celui du nouveau tableau d'une
		// taille supérieure de 10
		tab = newTab;
	}
}