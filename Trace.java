/**
 * Classe representant une Trace composee de Segments
 * 
 * @author Etienne Boucher et Marc-Andre Beaudoin
 *
 */
public class Trace {
	private Liste segments;

	/**
	 * Constructeur d'une Trace contenant seulement le segment en tete
	 * 
	 * @param tete
	 *            premier Segment de la Trace.
	 */
	public Trace(Segment tete) {
		segments = new ListeTableau();
		segments.append(tete);
	}

	/**
	 * Constructeur d'une Trace vide
	 */
	public Trace() {
		segments = new ListeTableau();
	}

	/**
	 * Methode qui allonge le debut de la Trace dans la direction indiquee en
	 * parametre
	 * 
	 * @param direction
	 *            direction dans laquelle ajouter un segment par rapport au
	 *            debut de la Trace
	 */
	public void allonge(char direction) {
		Point debut;
		Point fin;
		switch (direction) {
		case 'N':
			debut = new Point(this.tete().getX(), this.tete().getY() - 1);
			fin = this.tete();
			segments.preprend(new Segment(debut, fin));
			break;
		case 'S':
			debut = new Point(this.tete().getX(), this.tete().getY() + 1);
			fin = this.tete();
			segments.preprend(new Segment(debut, fin));
			break;
		case 'O':
			debut = new Point(this.tete().getX() - 1, this.tete().getY());
			fin = this.tete();
			segments.preprend(new Segment(debut, fin));
			break;
		case 'E':
			debut = new Point(this.tete().getX() + 1, this.tete().getY());
			fin = this.tete();
			segments.preprend(new Segment(debut, fin));
			break;
		}
	}

	/**
	 * Methode qui renvoie le Point en debut de Trace
	 * 
	 * @return Point en debut de Trace
	 */
	public Point tete() {
		return ((Segment) segments.getFirst()).getDebut();
	}

	/**
	 * Methode qui verifie si un point particulier est contenu dans la Trace
	 * 
	 * @param p
	 *            Point recherche
	 * @return true si le Point est dans la Trace, false sinon
	 */
	public boolean contient(Point p) {
		boolean in = p.equals(this.tete());
		for (int i = 0; in == false && i < segments.size(); i++) {
			in = p.equals(((Segment) segments.get(i)).getFin());
		}
		return in;
	}

	/**
	 * Methode qui retourne le ie segment de la Trace
	 * 
	 * @param i
	 *            position du segment a retourner
	 * @return le segement a la i<sup>e</sup> position
	 */
	public Segment get(int i) {
		return ((Segment) segments.get(i));
	}

	/**
	 * Méthode qui retourne la longueur de la Trace
	 * 
	 * @return longueur de la Trace
	 */
	public int length() {
		return segments.size();
	}
}
