import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 * Devoir 2, IFT1025, Hiver 2016
 * 
 * @author Etienne Boucher et Marc-Andre Beaudoin
 *
 */
public class Tron {

	/**
	 * Execution du programme
	 * 
	 * @param args
	 *            largeur et hauteur de grille de jeu
	 */
	public static void main(String[] args) {
		// variables
		int largeur_grille;
		int hauteur_grille;

		// Entrees par defaut ou entree par arguments pour hauteur et largeur de
		// grille.
		if (args.length == 0) {
			largeur_grille = 500;
			hauteur_grille = 500;
		} else {
			largeur_grille = Integer.parseInt(args[0]);
			hauteur_grille = Integer.parseInt(args[1]);
		}

		// Creation du frame principal et du TronPanel puis ajout au frame
		JFrame frame = new JFrame("TRON");
		frame.setBackground(Color.BLACK);
		TronPanel gamePanel = new TronPanel(largeur_grille, hauteur_grille);
		frame.add(gamePanel);

		// Parametres du frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

		// Localise la fenetre de jeu au millieu de l'ecran
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
	}
}