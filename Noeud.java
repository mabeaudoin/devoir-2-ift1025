/**
 * classe d'objets qui sont l'unité de {@link ListeChainee}
 * 
 * @author Etienne Boucher et Marc-André Beaudoin
 *
 */
public class Noeud {

	Object contenu;
	Noeud suivant;

	/**
	 * 
	 * @param contenu
	 * @param suivant
	 */
	public Noeud(Object contenu, Noeud suivant) {
		this.contenu = contenu;
		this.suivant = suivant;
	}

	/**
	 * méthode d'accès à l'Object contenu dans le Noeud
	 * 
	 * @return l'adresse de l'Object contenu dans le Noeud
	 */
	public Object getContenu() {
		return contenu;
	}

	/**
	 * méthode d'accès aux Noeud suivant dans la ListeChainee
	 * 
	 * @return l'adresse du Noeud suivant dans la ListeChainee
	 */
	public Noeud getSuivant() {
		return suivant;
	}

	/**
	 * méthode de modification du Noeud suivant dans la ListeChainee
	 * 
	 * @param suivant
	 *            Noeud par lequel on change le Noeud suivant
	 */
	public void setSuivant(Noeud suivant) {
		this.suivant = suivant;
	}
}