/**
 * Classe representant la position d'un point aux coordonnees x,y (attributs de
 * type int) de la grille virtuelle
 * 
 * @author Etienne Boucher et Marc-André Beaudoin
 *
 */
public class Point {
	private int x, y;

	/**
	 * Constructeur d'un Point en (x,y)
	 * 
	 * @param x
	 *            valeur en x du Point
	 * @param y
	 *            valeur en y du Point
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Accesseur de la position en x du Point
	 * 
	 * @return valeur de la position en x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Modificateur de la position en x du Point
	 * 
	 * @param x
	 *            position en x a attribuer au Point
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Accesseur de la position en y du Point
	 * 
	 * @return valeur de la position en y du Point
	 */
	public int getY() {
		return y;
	}

	/**
	 * Modificateur de la position en y du Point
	 * 
	 * @param y
	 *            position en y a attribuer au Point
	 */
	public void setY(int y) {
		this.y = y;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return (x == ((Point) obj).getX()) && (y == ((Point) obj).getY());
	}
	
	public String toString(){
		return "("+x+","+y+")";
	}
}