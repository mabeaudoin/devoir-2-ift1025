import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Sous classe de JPanel qui contient les scores de la partie de tron qui
 * s'intègre au {@link TronPanel}
 * 
 * @author Marc-André Beaudoin et Etienne Boucher
 *
 */
public class TronScorePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	// Création des JLables pour le TronScorePanel et mise en forme
	JLabel scoreMark = new JLabel("Scores");
	JLabel player1Label = new JLabel("Joueur 1");
	JLabel player1Score = new JLabel("" + 0);
	JLabel player2Label = new JLabel("Joueur 2");
	JLabel player2Score = new JLabel("" + 0);
	JLabel computerLabel = new JLabel("Ordinateur");
	JLabel computerScore = new JLabel("" + 0);

	/**
	 * Constructeur de TronScorePanel
	 * 
	 */
	public TronScorePanel() {
		// Mise en place du TronScorePanel
		BoxLayout boxlayout = new BoxLayout(this, BoxLayout.Y_AXIS);
		add(Box.createRigidArea(new Dimension(10, 10)));
		setLayout(boxlayout);
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		setPreferredSize(new Dimension(100, 100));
		setOpaque(true);
		setBackground(Color.BLACK);

		// Mise en forme des JLabel
		Font font1 = new Font("Courrier", Font.BOLD, 16);
		Font font2 = new Font("Courrier", Font.ITALIC, 14);
		scoreMark.setFont(font1);
		computerLabel.setFont(font2);
		player1Label.setFont(font2);
		player2Label.setFont(font2);
		scoreMark.setAlignmentX(Component.CENTER_ALIGNMENT);
		computerLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		computerScore.setAlignmentX(Component.CENTER_ALIGNMENT);
		player1Label.setAlignmentX(Component.CENTER_ALIGNMENT);
		player1Score.setAlignmentX(Component.CENTER_ALIGNMENT);
		player2Label.setAlignmentX(Component.CENTER_ALIGNMENT);
		player2Score.setAlignmentX(Component.CENTER_ALIGNMENT);
		scoreMark.setForeground(Color.WHITE);
		computerLabel.setForeground(Color.WHITE);
		computerScore.setForeground(Color.WHITE);
		player1Label.setForeground(Color.WHITE);
		player1Score.setForeground(Color.WHITE);
		player2Label.setForeground(Color.WHITE);
		player2Score.setForeground(Color.WHITE);
		
		// Ajout des JLabel au TronScorePanel
		add(scoreMark);
		add(computerLabel);
		add(computerScore);
		add(player1Label);
		add(player1Score);
		add(player2Label);
		add(player2Score);
	}

	/**
	 * methode qui ajoute des points aux pointages des joueurs
	 * 
	 * @param scores
	 *            un tableau de trois entiers correspondant aux points a
	 *            ajouter, dans l'ordre, au joueur1, au joueur 2 et a
	 *            l'ordinateur
	 */
	public void setScores(int[] updateScores) {
		computerScore.setText("" + updateScores[0]);
		player1Score.setText("" + updateScores[1]);
		player2Score.setText("" + updateScores[2]);
	}
}