import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Panneau de type {@link JPanel} qui permet de choisir les couleurs des Joueurs
 * au debut d'une partie
 * 
 * @author Marc-andre Beaudoin et Etienne Boucher
 *
 */
public class TronColorPanel extends JPanel {

	// variables locales
	private JLabel[] tagsJoueurs;
	private ColorComboBox[] comboBoxsJoueurs;
	private static final long serialVersionUID = 1L;

	/**
	 * constructeurs de TronColorPanel qui servent au choix de couleurs pour les
	 * joueurs incluant l'ordinateur
	 * 
	 * @param n
	 *            nombre de joueurs maximum pouvant participer simultanement
	 */
	public TronColorPanel(int n) {
		// Mise en place du TronColorPanel
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		setPreferredSize(new Dimension(100, 100));
		setOpaque(true);
		setBackground(Color.BLACK);

		// Creation et mise en forme des Jlabel et ColorComboBox pour le choix
		// des couleurs des joueurs
		tagsJoueurs = new JLabel[n];
		comboBoxsJoueurs = new ColorComboBox[n];
		Font font = new Font("Courrier", Font.ITALIC, 14);
		JPanel tempPanel = new JPanel();

		for (int i = 0; i < n; i++) {
			if (i == 0) {
				tagsJoueurs[i] = new JLabel("Ordinateur", JLabel.CENTER);
			} else {
				tagsJoueurs[i] = new JLabel("Joueur " + i, JLabel.CENTER);
			}
			comboBoxsJoueurs[i] = new ColorComboBox(i % 5);
			// Mise en forme des elements de texte
			tagsJoueurs[i].setFont(font);
			tagsJoueurs[i].setForeground(Color.WHITE);
			// Ajout des elements au Panel temporaire
			tempPanel.add(tagsJoueurs[i]);
			tempPanel.add(comboBoxsJoueurs[i]);
		}
		// Mise en forme du Panel temporaire et ajout au TronControlPanel
		tempPanel.setOpaque(true);
		tempPanel.setBackground(Color.BLACK);
		add(tempPanel);
	}

	/**
	 * Accesseur de la couleur du joueur a l'index demande 
	 * @param i index du joueur: Ordinateur si 0, Joueur i sinon
	 * @return la Color du joueur donne en index
	 */
	public Color getColor(int i) {
		return comboBoxsJoueurs[i].getColor();
	}
	
	/**
	 * @return comboBoxsJoueurs, le tableau de comboBoxs de choix de couleur des Joueurs
	 */
	public ColorComboBox[] getComboBoxsJoueurs() {
		return comboBoxsJoueurs;
	}
}